<div class="col-12 background-white round-all">
	<div class="row">
		<div class="col"></div>
		<div class="col-10 nopadd">
			<h3 class="center-text nomargin">{{ $move['translation']['name'] }}</h3>
		</div>
		<div class="col"></div>
	</div>
	<div class="row">
		<div class="col"></div>
		<div class="col-9">
			<img style="object-fit: contain;" class="img-fluid rounded mx-auto d-block" src="{{ $move['image']['link'] ?? '/pictures/move_default.jpg' }}"/>		
		</div>
		<div class="col"></div>
	</div>
	<div class="col-12">
		<p>{{ $move['translation']['description'] }}</p>
	</div>
	<div class="col-12">
		<h4 class="center-text">Toistoja: {{ $move['repetition'] }}</h4>
	</div>
</div>