<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title')</title>

        <!-- CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css">

        <!-- JS -->
        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js" integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js" integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc" crossorigin="anonymous"></script>
        {{-- Global scripts --}}
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        </script>
        @stack('before_scripts')
        <script>
        </script>
        @stack('after_scripts')

        <!-- Styles -->
        <style>
            .center-text{
                text-align: center;
            }
            .disclaimer{
                background-color: lightpink;
            }
            .nopadd{
                padding: 0px !important;
            }
            .nomargin{
                margin: 0px !important;
            }
            .background-white{
                background-color: white;
            }
            .round-all{
                border-radius: .5rem .5rem .5rem .5rem;   
            }
            .background-default-image{
                background-image: url('{{URL::to('/')}}/pictures/background_default.jpg'); 
                background-repeat: no-repeat; 
                background-attachment: fixed;
                background-size: cover;     
            }
            .sticky {
                position: fixed;
                top: 0;
                width: 100%;
                z-index: 100;
                height: 50px;
            }
            .navbar-top-padding{
                padding-top: 50px;
            }
            .container-fluid > a{
                color: white;
            }
            .no-link{
                cursor: default;
                text-decoration: none;
                color: black;
            }
            .no-link:hover{
                cursor: pointer;
                text-decoration: none;
                color: black;
            }
            .wrapper{
                display: flex;
                width: 100%;
                align-items: stretch;
            }
            #sidebar {
                padding-top: 50px;
                min-width: 250px;
                max-width: 250px;
                margin-left: -250px;
                min-height: 100vh;
                z-index: 99;
                transition: all 1s;
            }
            #sidebar.active {
                margin-left: 0px;
            }
            #navbar{
                z-index: 100;
            }
            .bar-background-color{
                background-color: #a8e49f;   
            }
            .font-color{
                color: black;
            }
            .font-color:hover{
                color: white;
            }
            .font-color-reverse{
                color: white;
            }
            .font-color-reverse:hover{
                color: black;
            }
            .bi-list{
                color: white;
            }
            .bi-list-dark{
                color: black;
            }
            .bar-element{
                display: block;
                padding: .5rem 1rem;
            }
            #object_container{
                padding-top: 25px;
            }
            .margin-bottom-25{
                margin-bottom: 25px;
            }
            .navbar{
                margin-bottom: 25px;
            }
            .visible{
                display: block;
            }
            .not-visible{
                display: none;
            }
            .hideable > span > .active{
                background-color: white;
                color: black;
            }
            .select_container{
                margin-bottom: 25px !important;
            }
        </style>
    </head>
    <body class="background-default-image navbar-top-padding">
        <div class="wrapper">
            <nav id="navbar" class="navbar navbar-default navbar-fixed-top sticky bar-background-color">
                <div id="desktop_navbar" class="d-none d-sm-block">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link font-color @if(isset($active) && $active == 'home') active @endif home" href="/">Training-helper</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link font-color @if(isset($active) && $active == 'workouts') active @endif workouts" href="/workouts">Treeniohjelmat</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link font-color @if(isset($active) && $active == 'moves') active @endif moves" href="/moves">Liikkeet</a>
                        </li>
                        {{-- Näytetään dropdownit, jos niitä on ja ollaan kirjauduttu --}}
                        @if(isset($dropdowns) && session('token'))
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle options font-color" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">Toiminnot</a>
                                <ul class="dropdown-menu">
                                    @foreach($dropdowns as $index => $dropdown)
                                        @include('partials.dropdown_element', $dropdown)

                                    @endforeach
                                </ul>
                            </li>
                        @endif
                        @if(session('token'))
                            <li class="nav-item">
                                <a class="nav-link font-color @if(isset($active) && $active == 'logout') active @endif logout" href="/logout">Kirjaudu ulos</a>
                            </li>
                        @else
                            <li class="nav-item">
                                <a class="nav-link font-color @if(isset($active) && $active == 'register') active @endif register" href="/register">Rekisteröidy</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link font-color @if(isset($active) && $active == 'login') active @endif login" href="/login">Kirjaudu sisään</a>
                            </li>
                        @endif
                    </ul>
                </div>
                <div id="mobile_navbar" class="d-block d-sm-none">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link no-link home" href="/">Training-helper</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle options font-color" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">Toiminnot</a>
                            <ul class="dropdown-menu">
                                <li class="base_option" style="display: none;"><a class="dropdown-item" href="javascript:;">__TEXT__</a></li>
                                {{-- <li><a class="dropdown-item" href="#">Another action</a></li> --}}
                                {{-- <li><a class="dropdown-item" href="#">Something else here</a></li> --}}
                                {{-- <li><hr class="dropdown-divider"></li> --}}
                            </ul>
                        </li>
                        <li id="sidebar_toggle" class="nav-item">
                            <a class="nav-link" href="javascript:;"><i class="bi bi-list"></i></a>
                        </li>
                    </ul>
                </div>
            </nav>
            <nav id="sidebar" class="navbar navbar-default navbar-fixed-top sticky d-block d-sm-none bar-background-color">
                <ul class="list-unstyled components">
                    <li class="hideable">
                        <span>
                            <a class="bar-element font-color no-link workouts" href="/workouts">Treeniohjelmat</a>
                        </span>
                    </li>
                    <li class="hideable">
                        <span>
                            <a class="bar-element font-color no-link moves" href="/moves">Liikkeet</a>
                        </span>
                    </li>                    
                    <li class="hideable">
                        <span>
                            <a class="bar-element font-color no-link register" href="/register">Rekisteröidy</a>
                        </span>
                    </li>
                    <li class="hideable">
                        <span>
                            <a class="bar-element font-color no-link login" href="/login">Kirjaudu</a>
                        </span>
                    </li>
                    <li class="hideable">
                        <span>
                            <a class="bar-element font-color no-link logout" href="javascript:;">Kirjaudu ulos</a>
                        </span>
                    </li>
                </ul>

                {{-- <div class="sidebar-header">
                    <h3>Bootstrap Sidebar</h3>
                </div>

                <ul class="list-unstyled components">
                    <p>Dummy Heading</p>
                    <li class="active">
                        <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Home</a>
                        <ul class="collapse list-unstyled" id="homeSubmenu">
                            <li>
                                <a href="#">Home 1</a>
                            </li>
                            <li>
                                <a href="#">Home 2</a>
                            </li>
                            <li>
                                <a href="#">Home 3</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">About</a>
                    </li>
                    <li>
                        <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Pages</a>
                        <ul class="collapse list-unstyled" id="pageSubmenu">
                            <li>
                                <a href="#">Page 1</a>
                            </li>
                            <li>
                                <a href="#">Page 2</a>
                            </li>
                            <li>
                                <a href="#">Page 3</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Portfolio</a>
                    </li>
                    <li>
                        <a href="#">Contact</a>
                    </li>
                </ul> --}}
            </nav>
            <div class="col-12 nopadd nomargin">
                {{-- <h1 class="center-text disclaimer">Sivu ei ole tietoturvallinen ja on demo vaiheessa, käytä omalla vastuulla</h1> --}}
                @yield('content')                
            </div>
        </div>
    </body>
</html>
