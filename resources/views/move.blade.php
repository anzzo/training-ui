@extends('layout')
@section('title', 'Liike')
@section('content')
	<div class="col-12 row nopadd nomargin">
		<div class="col-sm"></div>
		<div id="object_container" class="col-sm-6 col-xs-12">
			@include('partials.move_specific')
		</div>
		<div class="col-sm"></div>
	</div>
@endsection
@push('before_scripts')
	<script>
	</script>
@endpush
@push('after_scripts')
	<script>
	</script>
@endpush