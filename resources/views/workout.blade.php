@extends('layout')
@section('title', 'Treeniohjelma')
@section('content')
	<div class="col-12 row nopadd nomargin">
		<div class="col-sm"></div>
		<div id="object_container" class="col-sm-6 col-xs-12">
			@include('partials.workout_specific')
		</div>
		<div class="col-sm"></div>
	</div>
	<div id="child_container" class="col-12">
		@foreach($workout['moves']->chunk(3) as $index => $chunks)
			<div class="row">
				@foreach($chunks as $subIndex => $move)
					@include('partials.move', $move)
				@endforeach
			</div>
		@endforeach
	</div>
@endsection
@push('before_scripts')
	<script>
	</script>
@endpush
@push('after_scripts')
	<script>
	</script>
@endpush