# Yleistä
Vaatii toimiakseen training-api rajapinnan, joka löytyy Gitlab projekteistani.
Kaikki url:t löytyy /routes/web.php:stä. Verkkosivun omassa backendissä kutsutaan training-api rajapintaa.

# Näkymät
Näkymät löytyvät rescources/views-kansiosta. Nämä on toteutettu käyttäen laravelin blade moottoria, sekä JS-kirjastoa nimeltä jQuery.

# Tietokanta
Sivulla ei ole omaa tietokantaa, vaan kaikki toimii API:n kautta.

# Aloitus
Lataa projekti omalle koneelle ja:
* Päivitä kirjastot

