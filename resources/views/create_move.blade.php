@extends('layout')
@section('title', 'Luo uusi liike')
@section('content')
	<div class="col-12 row nopadd nomargin">
		<div class="col-sm"></div>
		<div class="col-sm-3 col-xs-12">
			<div class="col-12 row">
				<div class="col"></div>
				<h2 class=" col center-text background-white round-all">Luo uusi liike</h2>
				<div class="col"></div>
			</div>
			@foreach($languages as $index => $language)
				<div class="row background-white round-all">
					Nimi {{ $language['translation']['name'] }}:
					<input id="name_{{ $language['id'] }}" data-type="name" data-language-id="{{ $language['id'] }}" class="langi col-12" type="text" placeholder="Liikkeen nimi"/>
				</div>
				<br />
				<div class="row background-white round-all">
					Kuvaus {{ $language['translation']['name'] }}:
					<textarea rows="4" id="description_{{ $language['id'] }}" data-type="description" data-language-id="{{ $language['id'] }}"  placeholder="Liikkeen kuvaus" class="langi col-12"></textarea>
				</div>
				<br />
			@endforeach
			<br />
			<div class="row background-white round-all">
				Toistoja:
				<input type="number" id="repetition">
			</div>
			<br />
			<div class="row">
				<span id="create" class="btn btn-primary col-12">Luo liike</span>
			</div>
		</div>
		<div class="col-sm"></div>
	</div>
@endsection
@push('before_scripts')
	<script>
		$(document).ready(function(e){
			$(document).on('click', '#create', async function(e){
				var langis = {};
				$.each($('.langi'), function(index, element){
					var type        = $(element).attr('data-type');
					var language_id = $(element).attr('data-language-id');
					if(typeof langis[language_id] == 'undefined'){
						langis[language_id] = {};
					}
					if(typeof langis[language_id][type] == 'undefined'){
						langis[language_id][type] = [];
					}
					langis[language_id][type] = $(element).val();
				});

				var data = {
					langis: langis,
					repetition: $('#repetition').val()
				}
				try{					
					const result = await $.ajax({
						method: 'POST',
						url: '/create/move',
						data: data
					});
					console.log(result);
					if(result.success == 0){
						/* Kirjautuminen epäonnistui */
						throw "creating move failed";
					}else{
						window.location.href = '/moves/own';
					}
				}catch(error){
					console.error(error);
				}
			});
		});
	</script>
@endpush
@push('after_scripts')
	<script>
	</script>
@endpush