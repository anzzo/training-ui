<?php

if (!function_exists('r_collect')) {
    function r_collect($array){
    	if(is_object($array)){
    		$array = json_decode($array);
    		$array = json_encode($array, true);
    	}
    	if(is_array($array) && empty($array)){
    		return '';
    	}
    	if(is_array($array)){
    		return collect(array_map('r_collect', $array));
    	}
    	return $array;
    }
}