<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/* Landing page */
Route::get('/', 'HomeController@index');

/* User urls */
Route::get('/login',    'LoginController@index');
Route::get('/register', 'LoginController@register');
Route::post('/login',   'LoginController@login');
Route::get('/logout',   'LoginController@logout');

/* urls for testing */
Route::get('/test', 'DataController@request');

/* workout related urls */
Route::group(['prefix' => 'workouts'], function(){
	Route::get('/', 'WorkoutController@index');
	Route::get('/workout/{id}', 'WorkoutController@workout');
	Route::get('/create',       'WorkoutController@create');
	Route::get('/own',          'WorkoutController@own');
});

Route::group(['prefix' => 'create'], function(){
	Route::post('/{model}', 'DataController@postCreate');
});

/* move related urls */
Route::group(['prefix' => 'moves'], function(){
	Route::get('/', 'MoveController@index');
	Route::get('/move/{id}', 'MoveController@move');
	Route::get('/create',    'MoveController@create');
	Route::get('/own',       'MoveController@own');
});