<div class="col-4 margin-bottom-25">
	<div class="col-12 background-white round-all">
		<a class="no-link" href="/workouts/workout/{{ $workout['id'] }}">
			<div class="row">
				<div class="col"></div>
				<div class="col-10 nopadd">
					<h3 class="center-text nomargin">{{ $workout['translation']['name'] }}</h3>
				</div>
				<div class="col"></div>
			</div>
			<div class="row">
				<div class="col"></div>
				<div class="col-9">
					<img style="object-fit: contain;" class="img-fluid rounded mx-auto d-block" src="{{ $workout['image']['link'] ?? '/pictures/workout_default.jpg' }}"/>		
				</div>
				<div class="col"></div>
			</div>
			<div class="col-12">
				<p>{{ $workout['translation']['description'] }}</p>
			</div>
		</a>
	</div>
</div>