@extends('layout')
@section('title', 'Luo uusi ohjelma')
@section('content')
	<div class="col-12 row nopadd nomargin">
		<div class="col-sm"></div>
		<div class="col-sm-3 col-xs-12">
			<div class="col-12 row">
				<div class="col"></div>
				<h2 class=" col center-text background-white round-all">Luo uusi treeniohjelma</h2>
				<div class="col"></div>
			</div>
			@foreach($languages as $index => $language)
				<div class="row background-white round-all">
					Nimi {{ $language['translation']['name'] }}:
					<input id="name_{{ $language['id'] }}" data-type="name" data-language-id="{{ $language['id'] }}" class="langi col-12" type="text" placeholder="Ohjelman nimi"/>
				</div>
				<br />
				<div class="row background-white round-all">
					Kuvaus {{ $language['translation']['name'] }}:
					<textarea rows="4" id="description_{{ $language['id'] }}" data-type="description" data-language-id="{{ $language['id'] }}"  placeholder="Ohjelman kuvaus" class="langi col-12"></textarea>
				</div>
				<br />
			@endforeach
			<br />
			<div class="row background-white round-all">
				Ohjelman jakoisuus:
				<select id="partition">
					@for($i = 0; $i < 7; $i++)
						<option value="{{ ($i + 1) }}">{{ ($i + 1) }}krt/vko</option>
					@endfor
				</select>
			</div>
			<br />
			<div class="row background-white round-all">
				<div class="col-8">
					Valitse liike:
					<select class="move col-12">
						<option value="0" selected>Ei valintaa</option>
						@foreach($moves as $index => $move)
							<option value="{{ $move['id'] }}">{{ $move['translation']['name'] }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-4 d-flex justify-content-center align-items-center">
					<a href="javascript:;" target="_blank">
						<span style="visibility: hidden;" class="btn btn-primary col-12">Avaa</span>
					</a>
				</div>
			</div>
			<br />
			<div class="row">
				<span id="create_workout" class="btn btn-primary col-12">Luo ohjelma</span>
			</div>
		</div>
		<div class="col-sm"></div>
	</div>
@endsection
@push('before_scripts')
	<script>
		$(document).ready(function(e){
			$(document).on('change', '.move:not(".moved")', function(e){
				let $this   = $(this);
				let $parent = $this.parents('.row:first');
				let $btn    = $parent.find('span');
				let $clone  = $parent.clone();
				$this.addClass('moved');

				$btn.css('visibility', 'visible');
				$btn.parents('a:first').attr('href', "/moves/move/" + $this.val());

				$('#create_workout').parents('.row:first').before($clone);
				$('#create_workout').parents('.row:first').before('<br/>');
			});

			$(document).on('change', '.moved', function(e){
				let $this   = $(this);
				let $parent = $this.parents('.row:first');
				let $btn    = $parent.find('span');
				$btn.css('visibility', 'visible');
				$btn.parents('a:first').attr('href', "/moves/move/" + $this.val());
			});

			$(document).on('click', '#create_workout', async function(e){
				var moves = [];
				$.each($('.moved'), function(index, e){
					moves.push($(e).val());
				});

				var langis = {};
				$.each($('.langi'), function(index, element){
					var type        = $(element).attr('data-type');
					var language_id = $(element).attr('data-language-id');
					if(typeof langis[language_id] == 'undefined'){
						langis[language_id] = {};
					}
					if(typeof langis[language_id][type] == 'undefined'){
						langis[language_id][type] = [];
					}
					langis[language_id][type] = $(element).val();
				});

				var names = {};
				$.each($('.name'), function(index, e){
					names[$(e).attr('data-language-id')] = $(e).val();
				});

				var descriptions = {};
				$.each($('.description'), function(index, e){
					descriptions[$(e).attr('data-language-id')] = $(e).val();
				});

				var data = {
					langis: langis,
					partition: $('#partition').val(),
					moves: moves
				}
				try{					
					const result = await $.ajax({
						method: 'POST',
						url: '/create/workout',
						data: data
					});
					console.log(result);
					if(result.success == 0){
						/* Kirjautuminen epäonnistui */
						throw "creating workout failed";
					}else{
						window.location.href = '/workouts/own';
					}
				}catch(error){
					console.error(error);
				}
			});
		});
	</script>
@endpush
@push('after_scripts')
	<script>
	</script>
@endpush