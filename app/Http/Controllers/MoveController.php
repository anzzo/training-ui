<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \GuzzleHttp\Client;
use App\Http\Controllers\DataController;

class MoveController extends Controller
{

    private $data;
    private $controller;
    public function __construct(){
        $this->data = [];
        $this->data['active'] = 'moves';
        $this->data['dropdowns'] = [
            [
                'href' => '/moves/create',
                'text' => 'Luo liike'
            ],
            [
                'href' => '/moves/own',
                'text' => 'Näytä omat'
            ]
        ];

        $this->controller = new DataController;
    }
    public function index(){
        $moves = $this->controller->getMovesWith([], ['translation']);
        $moves = $moves->chunk(3);

        $this->data['moves']   = $moves;

        return  view('moves', $this->data)->render();
	}

	public function move($id){
        $move = $this->controller->getMovesWith([$id], ['translation']);
        $this->data['move']   = $move->first();

        return view('move', $this->data)->render();
	}

    public function create(){
        $languages = $this->controller->getLanguagesWith([], ['translation']);
        $this->data['languages'] = $languages;

        return view('create_move', $this->data);
    }

    public function own(){
        $moves = $this->controller->getOwnMovesWith(['translation']);
        $moves = $moves->chunk(3);

        $this->data['moves']   = $moves;

        return view('moves', $this->data)->render();   
    }
}