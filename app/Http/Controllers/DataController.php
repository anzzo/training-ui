<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class DataController extends Controller{

	public $params = [];
	public function request($method, $url, $params){
		$this->params = $params;
		if(session('token')){
            $this->params['api_token'] = session('token');
        }
		$res = $this->client->request($method, $url, [
            'query' => $this->params,
        ]);

        /* Get workout to readable state */
        $ret = json_decode($res->getBody(), true);
        $ret = r_collect($ret) ?: collect();
        return $ret;
	}

	public function getUser(){
        return $this->request('GET', 'authenticated/self', $this->params);
	}

	public function getMove($id = null){
        $this->params['terms'] = [
        	'where' => [
        		'id' => $id
    		]
        ];
        return $this->request('GET', 'public/move', $this->params);
	}

	public function getMoves($ids = []){
        if(!empty($ids)){
        	$this->params['terms'] = [
        		'where' => []
        	];
        	foreach($ids as $index => $id){
        		$this->params['terms']['where'][] = [
        			'id' => $id
        		];
        	}
        }
        return $this->request('GET', 'public/move', $this->params);
	}

	public function getMovesWith($ids = [], $relations = []){
        if(!empty($ids)){
        	$this->params['terms'] = [
        		'where' => []
        	];
        	foreach($ids as $index => $id){
        		$this->params['terms']['where']['id'] = $id;
        	}
        }
        $this->params['relations'] = $relations;
        return $this->request('GET', 'public/move', $this->params);
	}

	public function getOwnMovesWith($relations = []){
    	$this->params['terms'] = [
    		'where' => []
    	];
    	$user = $this->getUser();
		$this->params['terms']['where']['creator_user_id'] = $user['id'];
        $this->params['relations'] = $relations;
        return $this->request('GET', 'public/move', $this->params);
	}

	public function getOwnWorkoutsWith($relations = []){
    	$this->params['terms'] = [
    		'where' => []
    	];
    	$user = $this->getUser();
		$this->params['terms']['where']['creator_user_id'] = $user['id'];
        $this->params['relations'] = $relations;
        return $this->request('GET', 'public/workout', $this->params);
	}

	public function getLanguagesWith($ids = [], $relations = []){
        if(!empty($ids)){
        	$this->params['terms'] = [
        		'where' => []
        	];
        	foreach($ids as $index => $id){
        		$this->params['terms']['where']['id'] = $id;
        	}
        }

        $this->params['relations'] = $relations;
		return $this->request('GET', 'public/language', $this->params);
	}

	public function getWorkoutsWith($ids = [], $relations = []){
		if(!empty($ids)){
        	$this->params['terms'] = [
        		'where' => []
        	];
        	foreach($ids as $index => $id){
        		$this->params['terms']['where']['id'] = $id;
        	}
        }

        $this->params['relations'] = $relations;
		return $this->request('GET', 'public/workout', $this->params);
	}

    public function postCreate(Request $request, $model){
    	switch($model){
    		case 'workout':
    			$ret = $this->createWorkout($request);
    			break;
    		case 'move':
    			$ret = $this->createMove($request);
    			break;
    		default: throw new \Exception('No such create method');
    	}
    	return $ret;
    }

    public function createWorkout($request){
    	$workouts = $this->postWorkout($request);
    	$translations   = [];
    	$move_relations = [];
    	foreach($workouts as $index => $workout){
    		$translations[]   = $this->postWorkoutTranslations($request, $workout);
    		$move_relations[] = $this->postWorkoutMoveRelations($request, $workout);
    	}
    	return ['success' => 1, 'message' => 'Success', 'data' => [$workouts, $translations, $move_relations]];
    }

    public function postWorkout($request){
    	$user = $this->getUser();
    	$this->params['models'] = [];
    	$this->params['models'][] = [
    		'creator_user_id' => $user['id'],
    		'partition'       => $request->partition
    	];
        return $this->request('POST', 'authenticated/create/workout', $this->params);
    }

    public function postWorkoutTranslations($request, $workout){
    	$this->params['models'] = [];
    	foreach($request->langis as $language_id => $value){
	    	$this->params['models'][] = [
	    		'language_id' => $language_id,
	    		'workout_id'  => $workout['id'],
	    		'name'        => $value['name'],
	    		'description' => $value['description']
	    	];    		
    	}
		return $this->request('POST', 'authenticated/create/workout_translation', $this->params);
    }

    public function postWorkoutMoveRelations($request, $workout){
    	$this->params['models'] = [];
    	foreach($request->moves as $index => $move_id){
	    	$this->params['models'][] = [
	    		'workout_id' => $workout['id'],
	    		'move_id'    => $move_id,
	    	];
    	}
		return $this->request('POST', 'authenticated/create/workout_move_relation', $this->params);
    }

    public function createMove($request){
    	$moves = $this->postMove($request);
    	$translations = [];
    	foreach($moves as $index => $move){
    		$translations[] = $this->postMoveTranslations($request, $move);
    	}
    	return ['success' => 1, 'message' => 'Success', 'data' => [$moves, $translations]];
    }

    public function postMove($request){
    	$user = $this->getUser();
    	$this->params['models'] = [];
    	$this->params['models'][] = [
    		'creator_user_id' => $user['id'],
    		'repetition' => $request->repetition
    	];
        return $this->request('POST', 'authenticated/create/move', $this->params);
    }

    public function postMoveTranslations($request, $move){
    	$this->params['models'] = [];
    	foreach($request->langis as $language_id => $value){
	    	$this->params['models'][] = [
	    		'language_id' => $language_id,
	    		'move_id'     => $move['id'],
	    		'name'        => $value['name'],
	    		'description' => $value['description']
	    	];    		
    	}
		return $this->request('POST', 'authenticated/create/move_translation', $this->params);
    }
}
