@extends('layout')
@section('title', 'Rekisteröidy')
@section('content')
	<div class="col-12 row nopadd nomargin">
		<div class="col-sm"></div>
		<div class="col-sm-3 col-xs-12">
			<div class="col-12 row">
				<div class="col"></div>
				<h2 class=" col center-text background-white round-all">Rekisteröidy</h2>		
				<div class="col"></div>
			</div>
			<div class="row background-white round-all">
				Käyttäjänimi (pakollinen):
				<input id="user_name" class="col-12" type="text" placeholder="Käyttäjänimi"/>
			</div>
			<br />
			<div class="row background-white round-all">
				Etunimi:
				<input id="first_name" class="col-12" type="text" placeholder="Etunimi"/>
			</div>
			<br />
			<div class="row background-white round-all">
				Sukunimi:
				<input id="last_name" class="col-12" type="text" placeholder="Sukunimi"/>
			</div>
			<br />
			<div class="row background-white round-all">
				Syntymäpäivä:
				<input id="birthday" class="col-12" type="date" placeholder="Syntymäpäivä"/>
			</div>
			<br />
			<div class="row background-white round-all">
				Salasana (pakollinen):
				<input id="password" class="col-12" type="password" placeholder="Salasana"/>
			</div>
			<br />
			<div class="row background-white round-all">
				Salasana uudestaan (pakollinen):
				<input id="password_again" class="col-12" type="password" placeholder="Salasana"/>
			</div>
			<br />
			<div class="row">
				<span id="register" class="btn btn-primary col-12">Rekisteröidy</span>
			</div>
		</div>
		<div class="col-sm"></div>
	</div>
@endsection
@push('before_scripts')
	<script>
		$(document).ready(function(){

		});
	</script>
@endpush
@push('after_scripts')
	<script>
	</script>
@endpush