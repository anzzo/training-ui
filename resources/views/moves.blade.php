@extends('layout')
@section('title', 'Liikkeet')
@section('content')
	<div class="col-12 row nopadd nomargin">
		<div id="object_container" class="col-12">
			@foreach($moves as $index => $chunk)
				<div class="row">
					@foreach($chunk as $subIndex => $move)
						@include('partials.move', $move)
					@endforeach
				</div>
			@endforeach
		</div>
	</div>
@endsection
@push('before_scripts')
	<script>
	</script>
@endpush
@push('after_scripts')
	<script>
	</script>
@endpush