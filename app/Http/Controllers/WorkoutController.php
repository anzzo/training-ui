<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \GuzzleHttp\Client;
use App\Http\Controllers\DataController;

class WorkoutController extends Controller
{

	public function __construct(){
		$this->data = [];
        $this->data['active'] = 'workouts';
        $this->data['dropdowns'] = [
            [
                'href' => '/workouts/create',
                'text' => 'Luo treeniohjelma'
            ],
            [
                'href' => '/workouts/own',
                'text' => 'Näytä omat'
            ],
        ];

        $this->controller = new DataController;
	}
    public function index(){
	    $workouts = $this->controller->getWorkoutsWith([], ['translation']);
		$this->data['workouts'] = $workouts->chunk(3);

		return view('workouts', $this->data)->render();
	}

	public function workout($id){
	    $workout = $this->controller->getWorkoutsWith([$id], ['translation', 'moves', 'moves.translation']);
		$this->data['workout']  = $workout->first();

		return view('workout', $this->data)->render();
	}

	public function create(){
		$moves     = $this->controller->getMovesWith([], ['translation']);
		$languages = $this->controller->getLanguagesWith([], ['translation']);

        $this->data['moves']     = $moves;
        $this->data['languages'] = $languages;

		return view('create_workout', $this->data);
	}

	public function own(){
        $workouts = $this->controller->getOwnWorkoutsWith(['translation']);
    	$workouts = $workouts->chunk(3);

        $this->data['workouts']   = $workouts;

        return view('workouts', $this->data)->render();   
    }
}