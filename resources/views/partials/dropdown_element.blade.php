<li>
	<a class="dropdown-item" href="{{ $dropdown['href'] }}">
		{{ $dropdown['text'] }}
	</a>
</li>