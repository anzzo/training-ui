<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use \GuzzleHttp\Client;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /* Initialize Guzzle client so you don't have to do it everywhere */
    public $client = null;
    public function __construct(){
    	/* Apis base uri */
    	$base_uri = $base_uri ?? (env('API_URL') . "/api/v1/");

    	/* Create client */
        $this->client = new Client([
            'base_uri'   => "$base_uri",
        ]);
    }
}
