<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \GuzzleHttp\Client;

class LoginController extends Controller
{
    public function index(Request $request){
    	return view('login', ['active' => 'login']);
    }

    public function register(Request $request){
        return view('register', ['active' => 'register']);
    }

    public function login(Request $request){
    	try{
		    /* Send request to api */
		    $res = $this->client->request('GET', 'public/login', [
		    	'query' => [
		    		'email'    => $request->email,
		    		'password' => $request->password,
		    	]
		    ]);

		    /* Get content to readable state */
			$content = json_decode($res->getBody(), true);
			$content = r_collect($content);


			if($content['token'] === null) throw new \Exception('Wrong credentials');
			session(['token' => $content['token']]);
			return ['success' => 1, 'message' => '', 'data' => []];
    	}catch(\Exception $e){
    		info('Exception in LoginController@login');
    		info($e);
    		return ['success' => 0, 'message' => $e->getMessage(), 'data' => []];
    	}
    }

    public function logout(){
        /* Send logout request to api */
        $this->client->request('POST', 'public/logout', [
            'query' => [
                'api_token' => session('token')
            ]
        ]);
        /* Flush session */
    	session()->flush();
        /* And return to landing page */
    	return redirect('/');
    }
}
