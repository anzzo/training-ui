@extends('layout')
@section('title', 'Kirjaudu sisään')
@section('content')
	<div class="col-12 row nopadd nomargin">
		<div class="col-sm"></div>
		<div class="col-sm-3 col-xs-12">
			<div class="col-12 row">
				<div class="col"></div>
				<h2 class=" col center-text background-white round-all">Kirjaudu sisään</h2>		
				<div class="col"></div>
			</div>
			<div class="row background-white round-all">
				Sähköposti:
				<input id="email" class="col-12" type="text" placeholder="Sähköposti"/>				
			</div>
			<br />
			<div class="row background-white round-all">
				Salasana:
				<input id="password" class="col-12" type="password" placeholder="Salasana"/>
			</div>
			<br />
			<div class="row">
				<span id="login" class="btn btn-primary col-12">Kirjaudu</span>
			</div>
		</div>
		<div class="col-sm"></div>
	</div>
@endsection
@push('before_scripts')
	<script>
		let login = async function(){
			let $email    = $('#email');
			let $password = $('#password');
			try{				
				const result = await $.ajax({
					method: 'POST',
					url: '/login',
					data: {
						email: $email.val(),
						password: $password.val(),
					}
				});
				console.log(result);
				if(result.success == 0){
					/* Kirjautuminen epäonnistui */
					throw "Log in failed";
				}else{
					window.location.href = '/';
				}
			}catch(error){
				$email.val('');
				$password.val('');
				console.error(error);
			}
		}
		$(document).ready(function(){
			/* Listeners */
			$(document).on('click', '#login', login);
			$(document).on('keypress', '#password, #email', function(e) {
	    		if(e.which == 13) {
	        		login();
	    		}
			});		
		});
	</script>
@endpush
@push('after_scripts')
	<script>
	</script>
@endpush