@extends('layout')
@section('title', 'Treeniohjelmat')
@section('content')
	<div class="col-12 row nopadd nomargin">
		<div id="object_container" class="col-12">
			@foreach($workouts as $index => $chunks)
				<div class="row">
					@foreach($chunks as $subIndex => $workout)
						@include('partials.workout', $workout)
					@endforeach
				</div>
			@endforeach
		</div>
	</div>
@endsection
@push('before_scripts')
	<script>
	</script>
@endpush
@push('after_scripts')
	<script>
	</script>
@endpush